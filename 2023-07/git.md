## Gerrit账号信息

 **Gerrit URL**: http://100.2.48.223:81 

 **Username**: lvmengke 

 **Password**: $cccl//;a 



## 常用 **git** 命令

- **基础提交更新**

**git pull** //把远程库的代码更新到工作台

git pull --rebase origin master //强制把远程库的代码跟新到当前分支上面

**git fetch** //把远程库的代码更新到本地库

git add . //把本地的修改加到 stage 中

git commit -m 'comments here' //把 stage 中的修改提交到本地库

- **分支相关**

git branch 查看远程分支/全部分支，当前所处分支前面会有*

git branch dev01  创建dev01分支

**git checkout master/dev01**  //切换到某个分支，先查看，后切换，

**git checkout -b dev01**  //新建 dev01 分支，创建并切换到一个不存在的分支里

**git checkout -d dev01**  //删除 dev01分支

**git merge dev01**  //先切换到 master分支上面，把 dev01分支上的修改同步到 master分支上 

git merge tool //调用 merge 工具

- **缓存**

git stash //把未完成的修改缓存到栈容器中

git stash list //查看所有的缓存

git stash pop //恢复本地分支到缓存状态

- **日志**

git blame someFile //查看某个文件的每一行的修改记录（）谁在什么时候修改的）

**git status** //查看当前分支有哪些修改，**查看文件状态**

**git log** //查看当前分支上面的日志信息，**可查看到commit-id**

**git reflog** //查看每次操作的commit-id，**以免git reset到过去之后回不来！**

git diff //查看当前没有 add 的内容

git diff --cache //查看已经 add 但是没有 commit 的内容

git diff HEAD //上面两个内容的合并

**git diff HEAD -- readme.txt**  //命令可以查看工作区和版本库里面最新版本的区别

git show commit-id //显示某次 commit-id 的修改内容

**git reset --hard HEAD** //撤销本地修改**HEAD~x**，退回到**上x步**

**git reset --hard commit-id** //撤销 commit-id 前面的所有修改，**版本切换，退回**

git apply test.patch //打包 patch

![image-20230725081752566](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230725081752566.png)

![image-20230725082248416](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230725082248416.png)

## 分支

使用分支意味着你可以把你的工作从开发主线上分离开来进行重大bug的修改，在你自己的分支上开发新的功能，以免影响开发主线。



## 版本回退

`git log`命令显示从最近到最远的提交日志

在Git中，用`HEAD`表示当前版本。上一个版本就是`HEAD^`，上上一个版本就是`HEAD^^`，当然往上100个版本写100个`^`比较容易数不过来，所以写成`HEAD~100`

```bash
$ git reset --hard HEAD^
```

当你用`$ git reset --hard HEAD^`回退到`add distributed`版本时，再想恢复到`append GPL`，就必须找到`append GPL`的`commit id`。Git提供了一个命令`git reflog`用来记录你的每一次命令

```bash
$ git reset --hard 1094a
```

总结：

- `HEAD`指向的版本就是当前版本，因此，Git允许我们在版本的历史之间穿梭，使用命令`git reset --hard commit_id`。
- 穿梭前，用`git log`可以查看提交历史，以便确定要回退到哪个版本。
- 要重返未来，用`git reflog`查看命令历史，以便确定要回到未来的哪个版本



## 工作区和暂存区

Git的版本库里存了很多东西，其中最重要的就是称为stage（或者叫index）的暂存区，还有Git为我们自动创建的第一个分支`master`，以及指向`master`的一个指针叫`HEAD`。

![image-20230725173424293](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230725173424293.png)

把文件往Git版本库里添加的时候，是分两步执行的：

第一步是用`git add`把文件添加进去，实际上就是把文件修改添加到暂存区；

第二步是用`git commit`提交更改，实际上就是把暂存区的所有内容提交到当前分支。



## 撤销修改

```bash
git checkout -- readme.txt
```

把**`readme.txt`**文件在工作区的修改全部撤销，这里有两种情况：

一种是**`readme.txt`**自修改后还没有被放到暂存区，现在，撤销修改就回到和版本库一模一样的状态；

一种是**`readme.txt`**已经添加到暂存区后，又作了修改，现在，撤销修改就回到添加到暂存区后的状态。

`git reset HEAD <file>`可以把暂存区的修改撤销掉（unstage），重新放回工作区：

```bash
git reset HEAD  readme.txt
```

Git的版本回退速度非常快，因为Git在内部有个指向当前版本的`HEAD`指针，当你回退版本的时候，Git仅仅是把HEAD从指向`append GPL`

![image-20230725194718408](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230725194718408.png)

小结：

场景1：当你改乱了工作区某个文件的内容，想直接丢弃工作区的修改时，用命令`git checkout -- file`。

场景2：当你不但改乱了工作区某个文件的内容，还添加到了暂存区时，想丢弃修改，分两步，第一步用命令`git reset HEAD <file>`，就回到了场景1，第二步按场景1操作。

场景3：已经提交了不合适的修改到版本库时，想要撤销本次提交，参考[版本回退](https://www.liaoxuefeng.com/wiki/896043488029600/897013573512192)一节，不过前提是没有推送到远程库。



## 删除文件

在文件管理器中把文件删了

有两个选择，一是确实要从版本库中删除该文件，那就用命令`git rm`删掉，并且`git commit`：

```bash
$ git rm test.txt
$ git commit -m "remove test.txt"
```

另一种情况是删错了，因为版本库里还有呢，所以可以很轻松地把误删的文件恢复到最新版本：

```bash
$ git checkout -- test.txt
```

 **注意**：从来没有被添加到版本库就被删除的文件，是无法恢复的！



## 远程仓库

要本地作了提交，就可以通过命令：

```bash
$ git push origin master
```

把本地`master`分支的最新修改推送至GitHub，现在，你就拥有了真正的分布式版本库！

小结：

要关联一个远程库，使用命令`git remote add origin git@server-name:path/repo-name.git`；

关联一个远程库时必须给远程库指定一个名字，`origin`是默认习惯命名；

关联后，使用命令`git push -u origin master`第一次推送master分支的所有内容；

此后，每次本地提交后，只要有必要，就可以使用命令`git push origin master`推送最新修改；





# 分支管理

​		分支在实际中有什么用呢？假设你准备开发一个新功能，但是需要两周才能完成，第一周你写了50%的代码，如果立刻提交，由于代码还没写完，不完整的代码库会导致别人不能干活了。如果等代码全部写完再一次提交，又存在丢失每天进度的巨大风险。

​		现在有了分支，就不用怕了。你创建了一个属于你自己的分支，别人看不到，还继续在原来的分支上正常工作，而你在自己的分支上干活，想提交就提交，直到开发完毕后，再一次性合并到原来的分支上，这样，既安全，又不影响别人工作。

## 创建与合并分支

​		在[版本回退](https://www.liaoxuefeng.com/wiki/896043488029600/897013573512192)里，每次提交，Git都把它们串成一条时间线，这条时间线就是一个分支。截止到目前，只有一条时间线，在Git里，这个分支叫主分支，即`master`分支。`HEAD`严格来说不是指向提交，而是指向`master`，`master`才是指向提交的，所以，`HEAD`指向的就是当前分支。

​		一开始的时候，`master`分支是一条线，Git用`master`指向最新的提交，再用`HEAD`指向`master`，就能确定当前分支，以及当前分支的提交点：

![image-20230725191527703](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230725191527703.png)

​		每次提交，`master`分支都会向前移动一步，这样，随着你不断提交，`master`分支的线也越来越长。

​		当我们创建新的分支，例如`dev`时，Git新建了一个指针叫`dev`，指向`master`相同的提交，再把`HEAD`指向`dev`，就表示当前分支在`dev`上：

![image-20230725191613114](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230725191613114.png)

​		从现在开始，对工作区的修改和提交就是针对`dev`分支了，比如新提交一次后，`dev`指针往前移动一步，而`master`指针不变：

![image-20230725191647482](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230725191647482.png)

​		假如我们在`dev`上的工作完成了，就可以把`dev`合并到`master`上。Git怎么合并呢？最简单的方法，就是直接把`master`指向`dev`的当前提交，就完成了合并：

![image-20230725191808239](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230725191808239.png)

​		合并完分支后，甚至可以删除`dev`分支。删除`dev`分支就是把`dev`指针给删掉，删掉后，我们就剩下了一条`master`分支：



创建`dev`分支，然后切换到`dev`分支：

```bash
$ git checkout -b dev
```

`git checkout`命令加上`-b`参数表示创建并切换，相当于以下两条命令

```bash
$ git branch dev
$ git checkout dev
```

用`git branch`命令查看当前所有分支，所处分支前会有*号标识

我们把`dev`分支的工作成果合并到`master`分支上（必须先切换到master分支，被合并分支）：

```bash
$ git merge dev
```

合并完成后，就可以放心地删除`dev`分支了：

```bash
$ git branch -d dev
```

### switch

切换分支使用`git checkout <branch>`，撤销修改则是`git checkout -- <file>`

创建并切换到新的`dev`分支，可以使用：

```bash
$ git switch -c dev
```

直接切换到已有的`master`分支，可以使用：

```bash
$ git switch master
```

小结：

Git鼓励大量使用分支：

查看分支：`git branch`

创建分支：`git branch <name>`

切换分支：`git checkout <name>`或者`git switch <name>`

创建+切换分支：`git checkout -b <name>`或者`git switch -c <name>`

合并某分支到当前分支：`git merge <name>`

删除分支：`git branch -d <name>`



## 解决冲突

创建一个新分支`feature1`并在`master`分支与`feature1`分支对相同文件做不同修改，此时：

![image-20230725195307818](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230725195307818.png)

这种情况下，Git无法执行“快速合并”，只能试图把各自的修改合并起来，但这种合并就可能会有冲突。

```bash
$ git merge dev2
```

Git用`<<<<<<<`，`=======`，`>>>>>>>`标记出不同分支的内容，我们修改如下后保存再提交：

![image-20230725200128513](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230725200128513.png)

用带参数的`git log`也可以看到分支的合并情况：

```bash
$ git log --graph --pretty=oneline --abbrev-commit
```



## 分支管理策略

通常，合并分支时Git会用`Fast forward`模式，这种模式下，删除分支后，会丢掉分支信息（不知道何时添加过分支，分支在哪里）。

![image-20230726082139145](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230726082139145.png)

如图dev3消失了，强制禁用`Fast forward`模式，Git就会在merge时生成一个新的commit，这样，从分支历史上就可以看出分支信息。

```bash
$ git merge --no-ff -m "merge with no-ff" dev
```

![image-20230726082632989](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230726082632989.png)

不使用`Fast forward`模式，merge后就像这样：

![image-20230726082654212](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230726082654212.png)

**分支管理基本原则：**

​		`master`分支应该是非常稳定的，也就是仅用来发布新版本，平时不能在上面干活。干活都在`dev`分支上，也就是说，`dev`分支是不稳定的，到某个时候，比如1.0版本发布时，再把`dev`分支合并到`master`上，在`master`分支发布1.0版本。

​		每个人都在`dev`分支上干活，每个人都有自己的分支，时不时地往`dev`分支上合并就可以了。

![image-20230726084326765](C:\Users\lvmengke\AppData\Roaming\Typora\typora-user-images\image-20230726084326765.png)

​		合并分支时，加上`--no-ff`参数就可以用普通模式合并，合并后的历史有分支，能看出来曾经做过合并，而`fast forward`合并就看不出来曾经做过合并。



## Bug分支

​		每个bug都可以通过一个新的临时分支来修复，修复后，合并分支，然后将临时分支删除。当你接到一个修复一个代号101的bug的任务时，创建一个分支`issue-101`，但是当前正在`dev`上进行的工作还没有提交。工作只进行到一半，还没法提交，预计完成还需1天时间。但是，必须在两个小时内修复该bug。

​		Git还提供了一个`stash`功能，可以把当前工作现场“储藏”起来，等以后恢复现场后继续工作：

```bash
$ git stash
```

​		假定需要在`master`分支上修复，就从`master`创建临时分支，修复bug，切换到`master`分支，并完成合并，最后删除`issue-101`分支

```bash
$ git switch master
$ git switch -c issue-101
#修改bug
$ git add test.txt
$ git commit -m 'fix bug 101'
$ git switch master
$ git merge --no-ff -m 'merge bug 101' issue-101
$ git branch -d issue-101
```

​		接着回到`dev`分支干活了！Git把stash内容存在某个地方了，但是需要恢复一下，有两个办法，一是用`git stash apply`恢复，但是恢复后，stash内容并不删除，你需要用`git stash drop`来删除；另一种方式是用`git stash pop`，恢复的同时把stash内容也删了：

你可以多次stash，恢复的时候，先用`git stash list`查看，然后恢复指定的stash：

```bash
$ git stash apply stash@{0}
```

​		同样的bug，要在dev上修复，我们只需要把`4c805e2 fix bug 101`这个提交所做的修改“复制”到dev分支。注意：我们只想复制`4c805e2 fix bug 101`这个提交所做的修改，并不是把整个master分支merge过来。Git专门提供了一个`cherry-pick`命令，让我们能复制一个特定的提交到当前分支：

```bash
$ git cherry-pick 4c805e2
```

**以上存在部分问题！！！**



## 多人协作

`master`分支是主分支，因此要时刻与远程同步；

`dev`分支是开发分支，团队所有成员都需要在上面工作，所以也需要与远程同步

**推送分支:**

```
$ git push origin master
$ git push origin dev
```

**抓取分支:**

从远程库clone时，默认情况下，你的小伙伴只能看到本地的`master`分支。要在`dev`分支上开发，就必须创建远程`origin`的`dev`分支到本地.

```
$ git checkout -b dev origin/dev
```

**同时提交相同文件：**

你的小伙伴已经向`origin/dev`分支推送了他的提交，而碰巧你也对同样的文件作了修改，并试图推送。推送失败，因为你的小伙伴的最新提交和你试图推送的提交有冲突。先用`git pull`把最新的提交从`origin/dev`抓下来，然后，在本地合并，解决冲突，再推送：

```
$ git pull
```

`git pull`也失败了，原因是没有指定本地`dev`分支与远程`origin/dev`分支的链接，根据提示，设置`dev`和`origin/dev`的链接：

```
$ git branch --set-upstream-to=origin/dev dev
```

再pull：成功，但是合并有冲突，需要手动解决，解决的方法和分支管理中的[解决冲突](http://www.liaoxuefeng.com/wiki/896043488029600/900004111093344)完全一样。解决后，提交，再push：

#### **多人协作的工作模式**

1. 首先，可以试图用`git push origin <branch-name>`推送自己的修改；
2. 如果推送失败，则因为远程分支比你的本地更新，需要先用`git pull`试图合并；
3. 如果合并有冲突，则解决冲突，并在本地提交；
4. 没有冲突或者解决掉冲突后，再用`git push origin <branch-name>`推送就能成功！

如果`git pull`提示`no tracking information`，则说明本地分支和远程分支的链接关系没有创建，用命令`git branch --set-upstream-to <branch-name> origin/<branch-name>`。

#### 小结

- 查看远程库信息，使用`git remote -v`；
- 本地新建的分支如果不推送到远程，对其他人就是不可见的；
- 从本地推送分支，使用`git push origin branch-name`，如果推送失败，先用`git pull`抓取远程的新提交；
- 在本地创建和远程分支对应的分支，使用`git checkout -b branch-name origin/branch-name`，本地和远程分支的名称最好一致；
- 建立本地分支和远程分支的关联，使用`git branch --set-upstream branch-name origin/branch-name`；
- 从远程抓取分支，使用`git pull`，如果有冲突，要先处理冲突。



## Rebase

多人在同一个分支上协作时，很容易出现冲突。即使没有冲突，后push的童鞋不得不先pull，在本地合并，然后才能push成功。

```
$ git rebase
```

把分叉的提交历史“整理”成一条直线，看上去更直观。缺点是本地的分叉提交已经被修改过了。



# 标签管理

Git的标签虽然是版本库的快照，但其实它就是指向某个commit的指针（跟分支很像对不对？但是分支可以移动，标签不能移动），所以，创建和删除标签都是瞬间完成的。tag就是一个让人容易记住的有意义的名字，它跟某个commit绑在一起。

## 创建标签

首先，切换到需要打标签的分支上：然后，敲命令`git tag <name>`就可以打一个新标签：

```
$ git tag v1.0
```

默认标签是打在最新提交的commit上的。要对`add merge`这次提交打标签，它对应的commit id是`f52c633`，敲入命令：

```
$ git tag v0.9 f52c633
```

用命令`git tag`查看标签：

标签不是按时间顺序列出，而是按字母排序的。可以用`git show <tagname>`查看标签信息：

创建带有说明的标签，用`-a`指定标签名，`-m`指定说明文字：

```
$ git tag -a v0.1 -m "version 0.1 released" 1094adb
```



## 操作标签

如果标签打错了，也可以删除，创建的标签都只存储在本地，不会自动推送到远程。所以，打错的标签可以在本地安全删除：

```
$ git tag -d v0.1
```

要推送某个标签到远程，使用命令`git push origin <tagname>`：

```
$ git push origin v1.0
$ git push origin --tags  #一次性推送全部尚未推送到远程的本地标签：
```

如果标签已经推送到远程，要删除远程标签：

```
#先从本地删除
$ git tag -d v0.9
#从远程删除
$ git push origin :refs/tags/v0.9
```